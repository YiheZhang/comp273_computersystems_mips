# fileio.asm
	
.data

#Must use accurate file path.
#These file paths are EXAMPLES, 
#should not work for you
str1:	.asciiz "test1.txt"
str2:	.asciiz "test2.txt"
str3:	.asciiz "test.pgm"	#used as output
err:	.asciiz "error"
_P2:	.asciiz "P2\n"
_247:	.asciiz "24 7\n"
_15:	.asciiz "15\n"	

buffer:  .space 2048		# buffer for upto 2048 bytes

	.text
	.globl main

main:	la $a0,str1		#readfile takes $a0 as input
	jal readfile

	la $a0, str3		#writefile will take $a0 as file location
	la $a1,buffer		#$a1 takes location of what we wish to write.
	jal writefile

	li $v0,10		# exit
	syscall

readfile:
#Open the file to be read,using $a0
	li $v0, 13	
	li $a1, 0	#open for reading
	li $a2, 0	#mode unused
	syscall	
#Conduct error check, to see if file exists
	blt $v0, 0, error
# You will want to keep track of the file descriptor*
	move $s0, $v0	#save file descriptor
	
# read from file
# use correct file descriptor, and point to buffer
# hardcode maximum number of chars to read
# read from file

	li $v0, 14
	move $a0, $s0	#file descriptor
	la $a1, buffer	#load buffer
	li $a2, 2048	#hardcoded buffer length
	syscall
	
	blt $v0, 0, error	#checkfor error
	
# address of the ascii string you just read is returned in $v1.	
	
	move $v1, $a1

	li $v0, 4    	
	la $a0, buffer
	syscall
	
# the text of the string is in buffer

# close the file (make sure to check for errors)
	li   $v0, 16       
	move $a0, $s0      
	syscall
	
	blt $v0, 0, error	#check for error    
	
	jr $ra
	
writefile:
	move $s2, $a1	#save buffer
#open file to be written to, using $a0.
	li $v0, 13	
	li $a1, 1	#open for writing
	li $a2, 0	#mode unused
	syscall	
	move $s1, $v0	#move file descriptor 
	
#write the specified characters as seen on assignment PDF:
#P2
	li $v0, 15
	move $a0, $s1	#load descriptor
	la $a1, _P2
	li $a2, 3
	syscall
	blt $v0, 0, error	#check for error 
#24 7	
	li $v0, 15
	move $a0, $s1	#load descriptor
	la $a1, _247
	li $a2, 5
	syscall
	blt $v0, 0, error	#check for error 
#15
	li $v0, 15
	move $a0, $s1	#load descriptor
	la $a1, _15
	li $a2, 3
	syscall
	
	blt $v0, 0, error	#check for error 
#write the content stored at the address in $a1.
	li $v0, 15
	move $a0, $s1
	move $a1, $s2	#load buffer
	li $a2, 2048
	syscall
	
	blt $v0, 0, error	#check for error 	
#close the file (make sure to check for errors)
	li   $v0, 16       
	move $a0, $s1      
	syscall
	
	blt $v0, 0, error	#check for error  
	jr $ra

error:
	li $v0, 4     		#print error message
	la $a0, err
	syscall
exit:	
	li $v0, 10		#exit
	syscall
