#Calculator
#You will recieve marks based 
#on functionality of your calculator.
#REMEMBER: invalid inputs are ignored.
.data	
	array:  	.space 100
	int1:		.word 0
	int2:		.word 0
	operator:	.byte 1
	head:		.asciiz "Calculator for MIPS\nPress 'c' for clear and 'q' to quit : \n"
	result: 		.asciiz "\nthe result is : \n" 
	
	_0:		.float 0
	_0.5:		.float 0.5
	_100:		.float 100
	tempVal:		.space 200
	resVal:		.space 200
.text 
	
	.globl main

#TODO:
#main procedure, that will call your calculator
	
	li $t6, 0		#t6 is the result string pointer
	jal printHead
main:
			
	li $t9, 0		#array pointer
loop1:	
	jal read			#reads input
	move $a0, $v0		#moves input to $a0
	
	beq $a0, 113, quit	#if input = q, terminates the program
	beq $a0, 99, store	#if input = c, then it clears the previous result
	beq $a0, 42, store 	#if input = * 		
	beq $a0, 43, store	#if input = +
	beq $a0, 45, store	#if input = -
	beq $a0, 47, store	#if input = \
	beq $a0, 32, store 	#if input = space 
	beq $a0, 10, next   	#if input = enter, then it calls the funtion to do the calculation
	
	blt $a0, 48, loop1	#if input is not number, ignore
	bgt $a0, 57, loop1
	
store:	
	sb $a0, array($t9)	#stores current byte to an asscii array
	addi $t9, $t9, 1
		
	jal write
	j loop1

next:	
	sb $a0, array($t9)	#stores the current byte, enter, to the asscii array and then calls separate
	jal separate		#s5 stores integer 1, and s4 stores integer 2, and operator byte stores the operator
		
	mtc1 $s5, $f5    		#f4 = a1 = int1
	cvt.s.w $f5, $f5		#converts from word to float, then f5 = int 1
	mtc1 $s4, $f4
	cvt.s.w $f4, $f4		#converts from word to float, then f4 = int 2
	
	jal calculate		#do the calculation
	jal printResult		#then prints the result
	
	jal convert		
	jal printFloat		
		
	j main
	
##########################################this is a function that seperates all the elements in an asscii array
separate:
	li $t9, 0			#array pointer
	lb $t8, array($t9)		#t8 = current byte (first char)
	
	beq $t8, 99, reset		#if current byte = 99, resets all the stored value
	blt $t8, 48, case2		#if current byte < 48 which means it doesn't starts at a number

#number + operator + number
case1:
	li $s6, 1			#s6 = case number 1
	li $t7, 0			#set t7=0
	
	firstloop:
		lb $t8, array($t9)		#t8 = number
		beq $t8, 32, fisrtloopend	#if t8 = space, end first loop of case 1
		addi $t8, $t8, -48		#convert t8 from ascii to int
		
		mul $t7, $t7, 10		#t7*10
		add $t7, $t7, $t8
		
		add $t9, $t9, 1			#pointer increment by 1
		j firstloop
	fisrtloopend:
		move $s5, $t7			#####first int
		sw $t7, int1			#store case 1's first value	
	
	#now the pointer points to the space before the pointer
	addi $t9, $t9, 1			#pointer increment by 1
	lb $t8, array($t9)
	sb $t8, operator			#store the pointer
	addi $t9, $t9, 2			#pointer increment by 2
	
	#now the pointer points to the first char of the second value
	lb $t8, array($t9)			#t8 = number/sign
	li $t7, 0				#t7 = second value
	li $s7, 0				#s7=-1 if the value is negative
	
	bgt $t8, 47, secondloop		# if t8<48, then it is a negative number.
		li $s7, -1		# set s7=-1
		addi $t9, $t9, 1	# now the pointer points to a number
	secondloop:
		lb $t8, array($t9)	#t8=number
		beq $t8, 10, case1end	#if t8 = enter, end case 1
		addi $t8, $t8, -48	#convert t8 from ascii to int
		
		mul $t7, $t7, 10		#t7*10
		add $t7, $t7, $t8
		
		add $t9, $t9, 1		#pointer increment by 1
		j secondloop
	case1end:
		blt $s7, 0, c1negative	#check if the value is negative
			move $s4, $t7	#s4 = second int
			sw $t7, int2	#stores the in to int2.word [never used]
			jr $ra
		c1negative:
			mul $t7, $s7, $t7
			move $s4, $t7	#s4 = second int
			sw $t7, int2	#stores the in to int2.word [never used]
			li $s7, 0 	#set s7 back to 0
			jr $ra

#operator + number/sign + number		
case2:	 
	li $s6, 2			# s6=case number 2
	move $t7, $t8			# t7=operator
	addi $t9, $t9, 1			# moves to next to check if the char is a space 
	lb $t8, array($t9)		# now t8 gets the char after oprator
	
	bgt $t8, 47, case3		#if t8>48 then it is a number, go to case 3
					#else t8 = space, 
	sb $t7, operator			#saves the operator
	
	addi $t9, $t9, 1			#now the pointer points to a number/sign
	lb $t8, array($t9)		#t8 = number/sign
	
	li $t7, 0			#sets t7=0
	
	bgt $t8, 47, case2loop		#if t8<48, then it is a negative number.
		li $s7, -1		#set s7=-1
		addi $t9, $t9, 1		#now the pointer points to a number
	case2loop:
		lb $t8, array($t9)	#t8=number
		beq $t8, 10, case2end	#if t8 = enter, end case 2
		addi $t8, $t8, -48	#convert t8 from ascii to int
		
		mul $t7, $t7, 10		#t7*10
		add $t7, $t7, $t8
		
		add $t9, $t9, 1		#pointer increment by 1
		j case2loop
	case2end:
		blt $s7, 0, c2negative	#check if the value is negative
			move $s4, $t7	#s4 = second int
			sw $t7, int2	#stores the in to int2.word [never used]
			jr $ra
		c2negative:
			mul $t7, $s7, $t7
			move $s4, $t7	#s4 = second int
			sw $t7, int2	#stores the in to int2.word [never used]
			li $s7, 0 	#set s7 back to 0
			jr $ra

#sign + number + operator +...
#now the pointer points to the second char, and the first value is negative
case3:	
	li $s6, 3	#s6 = case number 3
	li $t9, 1
	
	li $s7, -1	#if s7 < 0, then the value is negative
	li $t7, 0	
	
	c3firstloop:
		lb $t8, array($t9)		#t8 = number
		beq $t8, 32, c3fisrtloopend	#if t8 = space, end first loop of case 3
		addi $t8, $t8, -48		#convert t8 from ascii to int
		
		mul $t7, $t7, 10			#t7*10
		add $t7, $t7, $t8
		
		add $t9, $t9, 1			#pointer increment by 1
		j c3firstloop
	c3fisrtloopend:
		mul $t7, $t7, -1
		move $s5, $t7			#s5 = fisrt int
		sw $t7, int1			#store case 3's first int
		li $s7, 0			#reset s7
	
	#now the pointer points to the space before the pointer
	addi $t9, $t9, 1			#pointer increment by 1
	lb $t8, array($t9)
	sb $t8, operator			#store the pointer
	addi $t9, $t9, 2			#pointer increment by 2
	
	#now the pointer points to the first char of the second value
	lb $t8, array($t9)			#t8 = number/sign
	li $t7, 0				#t7 = second value
	li $s7, 0				#s7=-1 if the value is negative
	
	bgt $t8, 47, c3secondloop		#if t8<48, then it is a negative number.
		li $s7, -1			#set s7=-1
		addi $t9, $t9, 1		#now the pointer points to a number
	c3secondloop:
		lb $t8, array($t9)		#t8=number
		beq $t8, 10, case3end		#if t8 = enter, end case 2
		addi $t8, $t8, -48		#convert t8 from ascii to int
		
		mul $t7, $t7, 10		#t7*10
		add $t7, $t7, $t8
		
		add $t9, $t9, 1			#pointer increment by 1
		j c3secondloop
	case3end:
		blt $s7, 0, c3negative	#check if the value is negative
			move $s4, $t7	#s4 = second int
			sw $t7, int2
			jr $ra
		c3negative:
			mul $t7, $s7, $t7
			move $s4, $t7	#s4 = second int
			sw $t7, int2
			li $s7, 0 	#set s7 back to 0
			jr $ra
#########################################################

#when user types c, reset every elements stored in 
reset:	
	li $s6, 4	#s6 = case number 4
	li $s4, 0
	li $s5, 0
	li $t7, 43
	sw $t7, operator 
	li $t7, 0
	jr $ra
	

#########################################################Calculates all the stored value
calculate: 	
	lb $t7, operator		#loads the operator byte to t7
	
	bne $s6, 2, start		#if the case number is not equal to 2, starts the calculation
	mov.s $f5, $f18		#first int = previous result

start:			
	beq $t7, 42, multiply	
	beq $t7, 43, plus
	beq $t7, 45, minus
	beq $t7, 47, divide
		
multiply:
	mul.s $f18, $f5, $f4
	jr $ra
plus:
	add.s  $f18, $f5, $f4
	jr $ra
minus:
	sub.s $f18, $f5, $f4
	jr $ra
divide:
	div.s $f18, $f5, $f4
	
	l.s $f29, _0.5
	l.s $f30, _100
	l.s $f31, _0
	mul.s $f18, $f18, $f30
	
	c.lt.s $f18, $f31		#if f18<0, go to negative
	bc1t negative
		add.s $f18, $f18, $f29
		j rounding
	negative:
		sub.s $f18, $f18, $f29
	rounding:
		cvt.w.s $f18, $f18	#converts .float to .word, and stores it in f18
  		mfc1 $t0, $f18		#t0 = (int)f18
		
		sw $t0, 0($sp)
		l.s $f18, 0($sp)
		cvt.s.w $f18, $f18
		
		div.s $f18, $f18, $f30
		
	jr $ra

###########################################################
printHead:
	addi $sp, $sp, -4		#saves the returned address
	sw $ra, 0($sp)
	
	loopHead:
	lb  $a0, head($t6)
	beq $a0, $zero, returnHead
	
        	jal write
        	addi $t6, $t6, 1
	j loopHead		
	
	returnHead:
		li $t6, 0
		lw $ra, 0($sp)
		addi $sp, $sp, 4
		jr $ra	

####################################################
printResult:
	addi $sp, $sp, -4		#save return address
	sw $ra, 0($sp)
	
	loopForResult:
		lb  $a0, result($t6)
		beq $a0, $zero, returnResult
        	jal write
        	addi $t6, $t6, 1	
		j loopForResult		
	returnResult:
		li $t6, 0
		lw $ra, 0($sp)			#load original ra jump back to the position in main
		addi $sp, $sp, 4
		jr $ra

#################################################this function converts result(int) to an ascii array
convert:	
	l.s $f30, _100
  	l.s $f31, _0
  	
  	c.eq.s $f18, $f31
  	bc1t equal0
  	
  	mul.s $f26, $f18, $f30	#multiply the original value by 100
  	cvt.w.s $f27, $f26	#converts .float to .word, and stores it in f1
  	mfc1 $t5, $f27 		#t5 = (int)f23
	
	blt $t5, 10, specialCase

	normalCase:	  	
  	#now stores the value to a temp ascii array
  	li $t7, 0		#t7 = temp array pointer
  	bgez $t5, resLoop1	#if result > 0, go to the function for negative value
  		
  		sub $t5, $zero, $t5	#converts t5 to positive
  		li $t6, -1		#t6=-1 iff t5<0
  		
  		resLoop1:
  			blt $t5, 10, resLoopEnd
  			
  			div $t5, $t5, 10	#t5 - t5/10
  			mfhi $t4		#t4 = t5 mod 10
  			
  			addi $t4, $t4, 48	#convert int to ascii
  			sb $t4, tempVal($t7)	#store current byte to a temp array
  			addi $t7, $t7, 1	#pointer increment by 1
  			j resLoop1
  		resLoopEnd:
  			bne, $t6, -1, posVal
  			
  			addi $t5, $t5, 48		#conver int to ascii
  			sb $t5, tempVal($t7)	#store the last byte to temp array
  			
  			addi $t7, $t7, 1
  			addi $t5, $zero, 45	#add a negative sign to the back
  			sb $t5, tempVal($t7)
  			
  			j resNext
  			
  		posVal:	addi $t5, $t5, 48		#conver int to ascii
  			sb $t5, tempVal($t7)	#store the last byte to temp array
  			
  			j resNext
  	
  	  	#everything is stored in tempVal, now saves it to resVal in reverse order	
  	resNext:
  		li $t8, 0	#a ponter for resVal
  		resLoop2:
  			beq $t7, 1, resLoop2End
  			
  			lb $t5, tempVal($t7)
  			sb $t5, resVal($t8)
  			
  			addi $t7, $t7, -1	#pointer for temp array decrement by 1
  			addi $t8, $t8, 1	#pointer increment by 1
  			j resLoop2
  		
  		resLoop2End:	
  			li $t6, 46
  			sb $t6, resVal($t8)	#store '.' to the result array
  			
  			addi $t8, $t8, 1	#pointer increment by 1
  			lb $t5, tempVal($t7)
  			sb $t5, resVal($t8)
  			
  			addi $t8, $t8, 1	#pointer increment by 1
  			addi $t7, $t7, -1	#pointer for temp array decrement by 1
  			lb $t5, tempVal($t7)
  			sb $t5, resVal($t8)
			
			addi $t8, $t8, 1	#pointer increment by 1
			li $t6, 10
			sb $t6, resVal($t8)	#store t6 to the result array		
			
			li $t4, 0
			li $t5, 0
			li $t6, 0
			li $t7, 0
			li $t8, 0	
		jr $ra
	
	#if flaoting point equals to zero
	equal0:	
		li $t8, 0	#a ponter for resVal
		li $t5, 48
		
		sb $t5, resVal($t8)
		addi $t8, $t8, 1
		li $t5, 10
		sb $t5, resVal($t8)
		
		li $t5, 0
		li $t8, 0
		
		jr $ra			
	
		specialCase:
		blt $t5, -9, normalCase
		blt $t5, 0, negitivecase
		
		li $t6, 48		#used as 0
		addi $t5, $t5, 48		#used as the last number
		li $t4,	46		#used as "."
		
		li $t8, 0		#a ponter for resVal
		sb $t6, resVal($t8) 	# store 0
		addi $t8, $t8, 1
		sb $t4, resVal($t8)	#store .
		addi $t8, $t8, 1
		sb $t6, resVal($t8)	#store 0
		addi $t8, $t8, 1
		sb $t5, resVal($t8)	#sotre the last number
		addi $t8, $t8, 1
		li $t5, 10		#store newline
		sb $t5, resVal($t8)
		
		li $t5, 0
		li $t6, 0
		li $t4, 0
		li $t8, 0
		jr $ra			
		
	negitivecase:
		li $t8, 0		#a ponter for resVal
		li $t4, 46		#used as "."
		li $t3, 45		#used as "-"
		li $t6, 48		#used as 0
		sub  $t5, $zero, $t5
		addi $t5, $t5, 48	#used as the last number
		
		sb $t3, resVal($t8)
		addi $t8, $t8, 1
		sb $t6, resVal($t8)
		addi $t8, $t8, 1
		sb $t4, resVal($t8)
		addi $t8, $t8, 1
		sb $t6, resVal($t8)
		addi $t8, $t8, 1
		sb $t5, resVal($t8)
		addi $t8, $t8, 1
		li $t5, 10
		sb $t5, resVal($t8)
		
		li $t5, 0
		li $t6, 0
		li $t4, 0
		li $t3, 0
		li $t8, 0
		jr $ra	

##########################################################
printFloat:
	addi $sp, $sp, -4		#save return address
	sw $ra, 0($sp)
	
	loopForFloat:
		lb  $a0, resVal($t6)
		beq $a0, 10, returnFloat
        	jal write
        	addi $t6, $t6, 1	
		j loopForFloat	
	returnFloat:
		addi $a0, $zero, 10
		jal write
		
		li $t6, 0
		lw $ra, 0($sp)			#load original ra jump back to the position in main
		addi $sp, $sp, 4
		jr $ra	

#################################################################
#a driver for getting input from MIPS keyboard
read:
	lui $t0, 0xffff
loop2:	lw $t1, 0($t0)	#checks for input
	andi $t1, $t1, 0x0001
	beq $t1, $zero, loop2	#if no input is entered by user, go back to loop1
	lw $v0, 4($t0)	#loads data
	jr $ra
	
#a driver for putting output to MIPS display
write:
	lui $t0, 0xffff
loop3:	lw $t1, 8($t0)	#check if ready for output
	andi $t1, $t1, 0x0001
	beq $t1, $zero, loop3
	sw $a0, 12($t0)	#stores value
	jr $ra

quit:			
	nop
