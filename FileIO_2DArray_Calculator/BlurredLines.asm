
.data

#Must use accurate file path.
#These file paths are EXAMPLES, 
#should not work for you
str1:	.asciiz "test1.txt"
str3:	.asciiz "test-blur.pgm"	#used as output

space:	.asciiz " "
err:	.asciiz "error"
_P2:	.asciiz "P2\n"
_247:	.asciiz "24 7\n"
_15:	.asciiz "15\n"

buffer:  .space 2048		# buffer for upto 2048 bytes
newbuff: .space 2048

array:	.word 200
	
	.text
	.globl main

main:	la $a0,str1		#readfile takes $a0 as input
	jal readfile


	la $a1,buffer		#$a1 will specify the "2D array" we will be averaging
	la $a2,newbuff		#$a2 will specify the blurred 2D array.
	jal blur


	la $a0, str3		#writefile will take $a0 as file location
	la $a1,newbuff		#$a1 takes location of what we wish to write.
	jal writefile

	li $v0,10		# exit
	syscall

readfile:
#done in Q1
#Open the file to be read,using $a0
	li $v0, 13	
	li $a1, 0	#open for reading
	li $a2, 0	#mode unused
	syscall	
#Conduct error check, to see if file exists
	blt $v0, 0, error
# You will want to keep track of the file descriptor*
	move $s0, $v0	#save file descriptor
# read from file
# use correct file descriptor, and point to buffer
# hardcode maximum number of chars to read
# read from file
	li $v0, 14
	move $a0, $s0	#file descriptor
	la $a1, buffer	#load buffer
	li $a2, 2048	#hardcoded buffer length
	syscall
	blt $v0, 0, error	#checkfor error
# address of the ascii string you just read is returned in $v1.	
	move $v1, $v0
	
# the text of the string is in buffer
# close the file (make sure to check for errors)
	li   $v0, 16       
	move $a0, $s0      
	syscall
	blt $v0, 0, error	#check for error    
	jr $ra
error:
	li $v0, 4     		#print error message
	la $a0, err
	syscall
exit:	
	li $v0, 10		#exit
	syscall

blur:
#use real values for averaging.
#HINT set of 8 "edge" cases.
#The rest of the averaged pixels will 
#default to the 3x3 averaging method
#we will return the address of our
#blurred 2D array in #v1

	sw $ra, 0($sp)
	
	move $s1, $a1	#move buffer to s1
	move $s2, $a2,	#move newbuffer to s2
	
	addi $t8, $t8, 0#array counter
	
	lb $t1, 0($s1)
	lb $t1, 0($s1)
	addi $t0, $t0, -1
	addi $t9, $t9, 1	#line counter
loop:
	beq $t1, ' ', skip1	#if t1=space, skip space
	beq $t1, '\n', skip2	#if t1=/n, skip line
	bgt $t1, 57, error	#if t1 is not a number, error
	blt $t1, 48, error
	bgtz $t0, cond		#if t0 has value, go to cond
	
	andi $t1, $t1, 0x0F	#convert ascii to integer
	add $t0, $t1, 0
	
	addi $s1, $s1, 1	#increment by 1
	lb $t1, 0($s1)
	j loop
cond:	
	bgt $t0, 255, error	#value greater than 255, error
	andi $t1, $t1, 0x0F	#convert ascii to integer

	mul $t0, $t0, 10	#muliple original value by 10
	add $t0, $t0, $t1	#add t1
	
	
	addi $s1, $s1, 1	#increment by 1
	lb $t1, 0($s1)
	j loop	
skip1:	
	bltz $t0, less1	#if t0 less than 0, go to less
			#else store the value
	
	sw $t0, array($t8)	#store t1 to array
	addi $t8, $t8, 4
	#addi $s3, $s3, 4
less1:
	addi $t0, $zero, -1
	addi $s1, $s1, 1	#increment by 1
	lb $t1, 0($s1)
	j loop
	
skip2:	
	bge $t9, 7, next1
	bltz $t0, less2	#if t0 less than 0, go to less
			#else store the value
	
	sw $t0, array($t8)	#store t1 to array
	addi $t8, $t8, 4
	#addi $s3, $s3, 4
less2:
	addi $t0, $zero, -1
	addi $s1, $s1, 1	#increment by 1
	lb $t1, 0($s1)
	addi $t9, $t9, 1	#line counter ++
	j loop	

###now every integer is stored in temp array
next1:	
	addi $t8, $zero, 0	#reset temp arraycounter to 0��increment by 4
	addi $t7, $zero, 0	#array pointer, increment by 1
	addi $t6, $zero, 10	#set t6=10
	addi $a3, $zero, 32	#a3=space
	addi $a2, $zero, 10	#a2=newline
	
saveBuffer:	
	beq $t7, 23, endline
	blt $t7, 25, edge
	beq $t7, 47, endline
	beq $t7, 71, endline
	beq $t7, 95, endline
	beq $t7, 119, endline
	beq $t7, 143, endline
	beq $t7, 167, endline
	beq $t7, 168, finish
	beq $t7, 48, edge
	beq $t7, 72, edge
	beq $t7, 96, edge
	beq $t7, 120, edge
	bge $t7, 144, edge
	j inner	#if the value is not on the edge, go to innner
	
edge:	
	lw $t0, array($t8)
	jal store
	addi $t8, $t8, 4
	sb $a3, 0($s2)	#add a space
	addi $s2, $s2, 1
	addi $t7, $t7, 1
	j saveBuffer
endline:
	lw $t0, array($t8)
	jal store
	addi $t8, $t8, 4
	sb $a2, 0($s2)	#add a newline
	addi $s2, $s2, 1
	addi $t7, $t7, 1
	j saveBuffer
inner:
	lw $t0, array($t8) #load current int to t0
	
	addi $t5, $t8, -100 #get the address of the upper left int
	lw $t1, array($t5)
	add $t0, $t0, $t1  #add this int to t0
	
	addi $t5, $t8, -96 #get the address of the upper int
	lw $t1, array($t5)
	add $t0, $t0, $t1  #add this int to t0
	
	addi $t5, $t8, -92 #get the address of the upper right corner
	lw $t1, array($t5)
	add $t0, $t0, $t1  #add this int to t0
	
	addi $t5, $t8, -4 #get the address of the left int
	lw $t1, array($t5)
	add $t0, $t0, $t1  #add this int to t0
	
	addi $t5, $t8, 4 #get the address of the right int
	lw $t1, array($t5)
	add $t0, $t0, $t1  #add this int to t0
	
	addi $t5, $t8, 92 #get the address of the lower left corner
	lw $t1, array($t5)
	add $t0, $t0, $t1  #add this int to t0
	
	addi $t5, $t8, 96 #get the address of the lower int
	lw $t1, array($t5)
	add $t0, $t0, $t1  #add this int to t0
	
	addi $t5, $t8, 100 #get the address of the lower right corner
	lw $t1, array($t5)
	add $t0, $t0, $t1  #add this int to t0
	
	div $t0, $t0, 9	#divide by 9
	
	mfhi $t5
	blt $t5, 5, noround	#if $t5>5, round
	addi $t0, $t0, 1	#get the rounded result
	
	noround:
	jal store	#store the value
	addi $t8, $t8, 4
	sb $a3, 0($s2)	#add a space
	addi $s2, $s2, 1
	addi $t7, $t7, 1
	j saveBuffer
	
store:
		bgt $t0, 9, store2	#if t0 is a 2/3 digits integer, go to store2
	
		addi $t0, $t0, 48		#convert t0 to ascii
		sb $t0, 0($s2)
		addi $s2, $s2, 1
		j return
	store2:	
		div $t0, $t6
		mflo $t0
		mfhi $t2
		
		bgt $t0, 9, store3	#if t0 is a 3 digits integer, go to store3
		
		addi $t0, $t0, 48		#convert t0 to ascii
		addi $t2, $t2, 48		#convert t2 to ascii
		
		sb $t0, 0($s2)	#store ascii to the new buffer
		sb $t2, 1($s2)
		addi $s2, $s2, 2
		j return
	store3:
		div $t0, $t6
		mflo $t0
		mfhi $t1
		
		addi $t0, $t0, 48		#conver int to ascii
		addi $t1, $t1, 48
		addi $t2, $t2, 48
		
		sb $t0, 0($s2)	#store ascii to the new buffer
		sb $t1, 1($s2)
		sb $t2, 2($s2)
		addi $s2, $s2, 3	
		j return
	return:	jr $ra

finish:
	la $v1, array
	lw $ra, 0($sp)
	jr $ra
	
writefile:
#done in Q1
	move $s2, $a1	#save buffer
#open file to be written to, using $a0.
	li $v0, 13	
	li $a1, 1	#open for writing
	li $a2, 0	#mode unused
	syscall	
	move $s1, $v0	#move file descriptor 
	
#write the specified characters as seen on assignment PDF:
#P2
	li $v0, 15
	move $a0, $s1	#load descriptor
	la $a1, _P2
	li $a2, 3
	syscall
	blt $v0, 0, error	#check for error 
#24 7	
	li $v0, 15
	move $a0, $s1	#load descriptor
	la $a1, _247
	li $a2, 5
	syscall
	blt $v0, 0, error	#check for error 
#15
	li $v0, 15
	move $a0, $s1	#load descriptor
	la $a1, _15
	li $a2, 3
	syscall
	
	blt $v0, 0, error	#check for error 
#write the content stored at the address in $a1.
	li $v0, 15
	move $a0, $s1
	move $a1, $s2	#load buffer
	li $a2, 2048
	syscall
	
	blt $v0, 0, error	#check for error 	
#close the file (make sure to check for errors)
	li   $v0, 16       
	move $a0, $s1      
	syscall
	
	blt $v0, 0, error	#check for error  
	jr $ra
