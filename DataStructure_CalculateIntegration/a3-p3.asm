# Name:Yihe Zhang
# Student ID: 260738383

# Problem 3
# Numerical Integration with the Floating Point Coprocessor
###########################################################
.data
N: .word 100
a: .float 3
b: .float 9
error: .asciiz "error: must have low < hi\n"
zero: .float 0
one: .float 1
pointFive: .float 0.5

five: .float 5
.text 
###########################################################
main:
	# set argument registers appropriately
	lwc1 $f12, a	#load lower and upper to f12 and f13
	lwc1 $f13, b
	# call integrate on test function 
	jal integrate
	# print result and exit with status 0
	li $v0, 2     		 
	mov.s $f12, $f0
	syscall
	
	li $v0, 17	#exit with status 0
	li $a0, 0
	syscall

###########################################################
# float integrate(float (*func)(float x), float low, float hi)
# integrates func over [low, hi]
# $f12 gets low, $f13 gets hi, $a0 gets address (label) of func
# $f0 gets return value
integrate: 
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	
	lwc1 $f12, a	#load lower and upper to f12 and f13
	lwc1 $f13, b
	
	######################################################
	la $a0, ident	#get the address of func
	######################################################
	
	jal check
	
	# initialize $f4 to hold N
	lw $a1, N		#save N to a1
	mtc1 $a1, $f4		#move a1 to f4
	cvt.s.w $f4, $f4	#convert word to float
	# since N is declared as a word, will need to convert 
	
	sub.s $f1, $f13, $f12	#f1=b-a
	div.s $f1, $f1, $f4	#delta x = f0=(b-a)/N
	
	l.s $f7, zero	#temp returned value
	l.s $f2, one	#f2=i
	l.s $f6, one	#1
	l.s $f26, pointFive	#f26=0.5
loop:	
	lwc1 $f12, a	#reload a to f12
	lwc1 $f24, a	#reload a to f22
	
	c.lt.s $f4, $f2	#if N<i, end the loop
	bc1t return	#if true, jump to return
	
	mul.s $f3, $f2, $f1	#f3=i*delta x
	add.s $f12, $f12, $f3	#f12= x(i) =a + i*delta x
	
	sub.s $f22, $f2, $f6	#f22=i-1
	mul.s $f23, $f22, $f1	#f23=(i-1)*delta x
	add.s $f24, $f24, $f23	#f24= x(i-1) =a + (i-1)*delta x
	
	add.s $f12, $f12, $f24 	#f12=x(i)+x(i-1)
	mul.s $f12, $f12, $f26	#f12=0.5*(x(i)+x(i-1))
	
	jalr $a0	#call function
	mov.s $f5, $f0	#move result to f5
	
	mul.s $f5, $f5, $f1 #f5 = f(x(i))*delta x
	add.s $f7, $f7, $f5 #temp returned value += f(x(i))*delta x
	
	add.s $f2, $f2, $f6	#i=i+1
	j loop
return:	
	mov.s $f0, $f7	#let f0  returned value
	
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	jr $ra

###########################################################
# void check(float low, float hi)
# checks that low < hi
# $f12 gets low, $f13 gets hi
# # prints error message and exits with status 1 on fail
check:	
	lwc1 $f12, a	#load lower and upper to f12 and f13
	lwc1 $f13, b
		
	c.lt.s $f13, $f12	#if upper<lower, status=1
	bc1t print		#if status=1, fail and go to print
	
	jr $ra
	
print:	
	li $v0, 4     		 #print error message
	la $a0, error
	syscall
	
	li $v0, 17	#exit with status 1
	li $a0, 1
	syscall

###########################################################
# float ident(float x) { return x; }
# function to test your integrator
# $f12 gets x, $f0 gets return value
ident:
	
	mov.s $f0, $f12
	jr $ra
