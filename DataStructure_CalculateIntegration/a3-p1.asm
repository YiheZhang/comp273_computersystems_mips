#StudentID:260738383
#Name:Yihe Zhang

.data
newline: .asciiz "\n"
askInput: .asciiz "Type a Character:"
original: .asciiz "Original linked list\n"
reversed: .asciiz "reversed linked list\n"
.text
#There are no real limit as to what you can use
#to implement the 3 procedures EXCEPT that they
#must take the specified inputs and return at the
#specified outputs as seen on the assignment PDF.
#If convention is not followed, you will be
#deducted marks.

main:
#build a linked list
	jal build
#print "Original linked list\n"
	li $v0, 4
	la $a0, original
	syscall
#print the original linked list
	move $a0, $v1
	jal print
#reverse the linked list
	move $a1, $v1
	jal reverse
#On a new line, print "reversed linked list\n"
	li $v0, 4
	la $a0, reversed
	syscall
#print the reversed linked list
	move $a0, $v1
	jal print
#terminate program
	j exit

build:
#continually ask for user input UNTIL user inputs "*"
#FOR EACH user inputted character inG, create a new node that hold's inG AND an address for the next node
#at the end of build, return the address of the first node to $v1
	
	addi $sp, $sp, -4	#save ra
	sw $ra, 0($sp)
	
	#linked list ==> [value][pointer]
	#create the first node
	li $a0, 8	#size of the node
	jal malloc	
	move $v1, $v0	#save the first node's address to $v1
	
	li $v0, 4
	la $a0, askInput
	syscall
	
	li $v0, 12      #read character
	syscall
	
	beq $v0, '*', inputNotValid #if input = *, go to inputNotValid
	sw $v0, 0($v1)	#save first node's value
	sw $zero, 4($v1)#node.next=zero
	
	move $t0, $v1	#let $t0 = $v1
	
	li $v0, 4
	la $a0, newline
	syscall
loop:	
	li $v0, 4
	la $a0, askInput
	syscall
	
	li $v0, 12      #read character
	syscall
	
	beq $v0, '*', return #if input = *, return
	move $t2, $v0	#move input value to $t2
	
	#create a temp node
	li $a0, 8	#size of the node
	jal malloc
	move $t1, $v0	#save tempNode's address to $t1
	
	sw $t1, 4($t0)	#save tempNode's address to previous node's pointer
	sw $t2, 0($t1)	#tempNode.value=input char
	sw $zero, 4($t1)#tempNode.next=zero
	
	move $t0, $t1	#t1 now equals to tempNode's address
	
	li $v0, 4
	la $a0, newline
	syscall
	j loop
#malloc function
malloc:
	li $v0, 9
	syscall
	jr $ra
inputNotValid:
	sw $zero, 0($v1)#save first node's value
	sw $zero, 4($v1)#node.next=zero
	j exit		#exit

return:
	li $v0, 4
	la $a0, newline
	syscall
	
	lw $ra, 0($sp)	#load original ra
	addi $sp, $sp, 4
	jr $ra

print:
#$a0 takes the address of the first node
#prints the contents of each node in order
	addi $sp, $sp, -4	#save ra
	sw $ra, 0($sp)

	move $a1, $a0	#move a0 to v0
	
	lw $t1, 4($a1)	#$t2 = nextNode's address
	
	
	lw $a0, 0($a1)	#load first node's value
	li $v0, 11	#print character
	syscall
	
loop2:	
	beq $t1, $zero, return	#if node.next=null, return
	lw $a0, 0($t1)
	lw $t1, 4($t1)
	
	li $v0, 11	#print character
	syscall
	
	j loop2



reverse:
#$a1 takes the address of the first node of a linked list
#reverses all the pointers in the linked list
#$v1 returns the address
	
	lw $t1, 4($a1)		#$t1=firstNode.next
	sw $zero, 4($a1)	#firstNode.next=null
	
	move $t0, $a1		#$t1=previousNode's address
loop3:
	beq $t1, $zero, move	#if previousNode.next=null, go to
	lw $t2,4($t1)	#$t2=currentNode.next
	sw $t0,4($t1)	#currentNode.next=previousNode's address
	move $t0, $t1
	move $t1, $t2
	
	j loop3		
move:
	move $v1, $t0	#set v1 = returned value
	jr $ra		#return

exit:
	li $v0, 10	#terminate
	syscall