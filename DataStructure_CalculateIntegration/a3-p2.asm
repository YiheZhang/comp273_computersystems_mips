# Name:Yihe Zhang
# Student ID: 260738383

# Problem 2 - Dr. Ackermann or: How I Stopped Worrying and Learned to Love Recursion
###########################################################
.data
error: .asciiz "error: m, n must be non-negative integers\n"
str1: 	.asciiz "Enter m: "
str2: 	.asciiz "Enter n: "  

.text 
###########################################################
main:
# get input from console using syscalls

	li $v0, 4      #Print Str1
	la $a0, str1
	syscall
	
	li $v0, 5      #Get int1
	syscall
	move $s1, $v0
	
	li $v0, 4	#Print Str2
	la $a0, str2
	syscall
	
	li $v0, 5	#Get int2
	syscall
	move $s2, $v0
	
	move $a1, $s1	#move stored values to $a1 and $a2
	move $a2, $s2
	
# compute A on inputs 
	jal A
# print value to console and exit with status 0

	move $a0,$v0
   	li $v0,1     
  	syscall 

	li $v0, 17	#exit with status 0
	li $a0, 0
	syscall
###########################################################
# int A(int m, int n)
# computes Ackermann function
A: 	
	addi $sp, $sp, -12	# create space for two items on the stack
	sw $ra, 8($sp)				
	sw $a2, 4($sp)
	sw $a1, 0($sp)
	jal check		#check m and n are natural numbers
	
	bgtz $a1, case1 	#if m>0, go to case 1
	
baseCase:
	addi $a2, $a2, 1	#n=n+1
	move $v0, $a2		#let $v0 be the returned value
	j return
case1:
	bgtz $a2, case2		#if n>0, go to case 2
	addi $a1, $a1, -1	#m=m-1
	addi $a2, $zero, 1	#n=1
	jal A			#call A(m-1,1)
	j return
case2:
	addi $a2, $a2, -1	#n=n-1
	jal A			#call A(m,n-1)
	move $a2, $v0		#a2 = returned value
	addi $a1, $a1, -1	#m=m-1
	jal A			#call A(m?1,A(m,n?1)) 
return:
	lw $ra, 8($sp)
	lw $a2, 4($sp)
	lw $a1, 0($sp)
	addi $sp, $sp, 12
	jr $ra
	
###########################################################
# void check(int m, int n)
# checks that n, m are natural numbers
# prints error message and exits with status 1 on fail
check:	bltz $a1, Print		#check the conditions
	bltz $a2, Print
	jr $ra
	
Print:	li $v0, 4     		 #print error message
	la $a0, error
	syscall
Exit:	li $v0, 17		#exit with status 1
	li $a0, 1
	syscall