# Program to capitalize the first letter of a string

	.data		# variable declarations follow this line
str: 	.asciiz "Enter the string to capitalize: "
input:	.space 127               
														
	.text		# instructions follow this line	


																	                    
main:     		# indicates start of code to test "upper" the procedure

	li $v0, 4      #Prints Str
	la $a0, str
	syscall
	
	li $v0, 8	#inputs string
	la $a0, input
	li $a1, 127
	syscall
	
	addi $t0, $t0, 0

loop:	lb $t1, input($t0)	#loads each letter into $t1 
	beq $t1, 0, exit	#if $t1==null, print and exit
	beq $t1, 32, plus1	#if $t1==space, pointer +1 and check if this letter needs convertion
	beq $t0, 0, upper	#check if the first letter needs convertion
	
	j goback
		
plus1:	addi $t0, $t0, 1	#pointer plus 1 and load the letter into $t1
	lb $t1, input($t0)
upper:	blt $t1, 97, goback	#check the if this letter is lower case
	bgt $t1, 122, goback
	sub $t1, $t1, 32	#converts the letter to upper case
	sb $t1, input($t0)	#stores the converted letter
	j goback
	
goback:	addi $t0, $t0, 1	#pointer plus 1 and go back to the loop
	j loop

exit:	li $v0, 4		#prints the result
    	la $a0, input
    	syscall
   	nop



									
# End of program
