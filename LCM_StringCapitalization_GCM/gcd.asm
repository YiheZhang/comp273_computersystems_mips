# Program to implement the Dijkstra's GCD algorithm

	.data		# variable declarations follow this line
str1: 	.asciiz "Enter the first integer: "
str2: 	.asciiz "Enter the second integer: "                  
														
	.text		# instructions follow this line	
	
main:     		# indicates start of code to test lcm the procedure

	li $v0, 4      #Print Str1
	la $a0, str1
	syscall
	
	li $v0, 8      #Enter Int1
	syscall
	move $s1, $v0
	
	
	li $v0, 4      #Print Str2
	la $a0, str2
	syscall
	
	li $v0, 8      #Enter Int2
	syscall
	move $s2, $v0

	
	move $a1, $s1
	move $a2, $s2
	
	jal gcd
	
	move $s3, $v0
			
	li $v0, 1	#Print
	add $a0, $zero, $s3
    	syscall
    													                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                    
	j Exit

gcd:	     		# the? procedure	
	
	addi $sp,$sp,-12	# create space for two items on the stack
	sw $ra,8($sp)				
	sw $a2,4($sp)
	sw $a1,0($sp)
	
	beq $a1,$a2,base	#if m==n, go to base case
	slt $t0, $a2, $a1	#if m>n go to case 1
	bnez $t0, case1
	beqz $t0, case2		#if m<n go to case 2
	
	jr $ra
case1:	sub $a1, $a1, $a2	#calculation
	jal gcd
	
	lw $ra,8($sp)				
	lw $a2,4($sp)
	lw $a1,0($sp)
	addi $sp,$sp,12
	
	jr $ra

case2:	sub $a2, $a2, $a1	#calculation
	jal gcd
	
	lw $ra,8($sp)				
	lw $a2,4($sp)
	lw $a1,0($sp)
	addi $sp,$sp,12
	
	jr $ra
	
base:	move $v0, $a1		#move $a1 to $v0
	
	lw $ra,8($sp)				
	lw $a2,4($sp)
	lw $a1,0($sp)
	addi $sp,$sp,12
	
	jr $ra

Exit:	nop


									
# End of program
