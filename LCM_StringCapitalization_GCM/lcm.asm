# Program to calculate the least common multiple of two numbers

	.data		# variable declarations follow this line
first: 	.word 10	# the first integer
second: .word 15    	# the second integer                  
														
	.text		# instructions follow this line	
																	                    
main:     		# indicates start of code to test lcm the procedure

	
	lw $t1, first	#get the value of first and second
	lw $t2, second
	
	move $s1, $t1	#store first and second value to s1 and s2
	move $s2, $t2
	
	move $a1, $s1
	move $a2, $s2
	
	jal gcd
	
	move $s3, $v0
	
	mul $t0, $s1, $s2	#first * second / gcd(first, second)
	div $s4, $t0, $s3	#store the result to $s4
			
	li $v0, 1	#Print
	add $a0, $zero, $s4
    	syscall
    													                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                        																	                    																	                    																	                    																	                    
	j Exit

gcd:	     			#the procedure	
	
	addi $sp,$sp,-12	# create space for two items on the stack
	sw $ra,8($sp)				
	sw $a2,4($sp)
	sw $a1,0($sp)
	
	beq $a1,$a2,base	#if m==n, go to base case
	slt $t0, $a2, $a1	#if m>n go to case 1
	bnez $t0, case1
	beqz $t0, case2		#if m<n go to case 2
	
	jr $ra
case1:	sub $a1, $a1, $a2	#calculation
	jal gcd
	
	lw $ra,8($sp)				
	lw $a2,4($sp)
	lw $a1,0($sp)
	addi $sp,$sp,12
	
	jr $ra

case2:	sub $a2, $a2, $a1	#calculation
	jal gcd
	
	lw $ra,8($sp)				
	lw $a2,4($sp)
	lw $a1,0($sp)
	addi $sp,$sp,12
	
	jr $ra
	
base:	move $v0, $a1		#move $a1 to $v0
	
	lw $ra,8($sp)				
	lw $a2,4($sp)
	lw $a1,0($sp)
	addi $sp,$sp,12
	
	jr $ra

Exit:	nop


									
# End of program










									
# End of program
